name := """http-aggregator"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

// Compile the project before generating Eclipse files, so that generated .scala or .class files for views and routes are present
// see https://www.playframework.com/documentation/2.5.x/IDE#Setup-sbteclipse
EclipseKeys.preTasks := Seq(compile in Compile)
