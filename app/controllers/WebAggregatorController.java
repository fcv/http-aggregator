package controllers;

import akka.NotUsed;
import akka.stream.Materializer;
import akka.stream.javadsl.Concat;
import akka.stream.javadsl.Merge;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.http.HttpEntity;
import play.libs.ws.StreamedResponse;
import play.libs.ws.WSClient;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import static akka.stream.javadsl.Source.combine;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

/**
 * <p>
 * Controller that performs HTTP requests to arbitrary URLs, join their HTTP response bodies and redirect to
 * requesting client.
 * </p>
 * <p>
 * Implementation based on Play Framework's "The Play WS API" documentation
 * page.
 * </p>
 *
 * @see <a href="https://www.playframework.com/documentation/2.5.x/JavaWS">https://www.playframework.com/documentation/2.5.x/JavaWS</a>
 * @author veronez
 *
 */
public class WebAggregatorController extends Controller {

	private static final Logger logger = LoggerFactory.getLogger(WebAggregatorController.class);

	@Inject
	private WSClient ws;

	/**
	 * Controller handler implementation using {@link StreamedResponse} based
	 * on https://www.playframework.com/documentation/2.5.x/JavaWS#Processing-large-responses
	 *
	 */
	public CompletionStage<Result> aggregate() {
		logger.debug("aggregate()");

		// original urls were 'http://www.google.com/', '// http://www.coolgames.com/' and
		// 'http://www.slasdot.org/'.. however it seems http://www.slasdot.org/ is for sale =(
		// using 'http://java.com' instead
		// TODO: consider allowing define these URL using HTTP request parameters
		List<String> urls = asList("http://www.google.com/", "http://www.coolgames.com/", "http://java.com");

		Stream<CompletionStage<Source<ByteString, ?>>> bodyStages = urls.stream()
				.map(url -> {
					// Note: I've tried using `.withRequestFilter` to add a filter that logs information about request
					// and the thread performing it... but it seems that filter is never called when using `.stream`
					// It is called when using `.get()`
					logger.debug("creating URL request to {}", url);
					// Using .stream instead of .get aiming to stream remote responses back to requesting client avoiding
					// whole content of all responses to be loaded into memory over burden our server
					// see https://www.playframework.com/documentation/2.5.x/JavaWS#Processing-large-responses
					return ws.url(url).stream();
				})
				.map(stage -> stage.thenApply(response -> {
					logger.debug("mapping CompletationState of StreamedResponse to CompletationState of Source<ByteString, ?> (response's body)");
					// TODO: consider checking HTTP response code for errors
					return response.getBody();
				}))
				;

		Optional<CompletionStage<Result>> result = bodyStages
				.reduce(WebAggregatorController::combineBodyStages)
				.map(s -> s.thenApply(content -> {

							logger.debug("mapping stage to result");
							return ok().sendEntity(new HttpEntity.Streamed(
									content,
									Optional.empty(),
									Optional.empty()
							));
						})
				);

		// by now it is safe to just call .get 'cause since we always have at least three urls the Optional resulting
		// of them will always have a value
		return result.get();
	}

	private static Source<ByteString, ?> combineBodies(Source<ByteString, ?> body1, Source<ByteString, ?> body2) {

		logger.debug("mergeBodies(body1: {}, body2: {})", "body1", "body2");
		// combining Sources based on documentation
		// http://doc.akka.io/docs/akka/2.4.12/java/stream/stream-graphs.html#Combining_Sources_and_Sinks_with_simplified_API
		List<Source<ByteString, ?>> rest = emptyList();
		Source<ByteString, NotUsed> combined = combine(body1, body2, rest, i -> {
			// using merge = false always by now
			// this behaviour might be customized by a http param in the future
			boolean merge = false;
			if (merge) {
				return Merge.<ByteString> create(i);
			} else {
				return Concat.<ByteString> create(i);
			}
		});
		return combined;
	}

	private static CompletionStage<Source<ByteString, ?>> combineBodyStages(
			CompletionStage<Source<ByteString, ?>> bodyStage1, CompletionStage<Source<ByteString, ?>> bodyStage2) {

		logger.debug("combineBodyStages(bodyStage1: {}, bodyStage2: {})", "bodyStage1", "bodyStage2");
		return bodyStage1.thenCombine(bodyStage2, WebAggregatorController::combineBodies);
	}

}
