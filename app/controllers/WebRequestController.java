package controllers;

import static play.mvc.Http.Status.OK;
import static play.mvc.Http.Status.BAD_GATEWAY;
import static java.util.Arrays.asList;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import akka.stream.impl.io.OutputStreamSourceStage;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.api.mvc.Headers;
import play.http.HttpEntity;
import play.libs.ws.*;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.util.ByteString;



/**
 * <p>
 * Controller that performs an Web Request to an arbitrary RSS and returns its
 * title value.
 * </p>
 * <p>
 * Implementation based on Play Framework's "The Play WS API" documentation
 * page.
 * </p>
 * 
 * @see https://www.playframework.com/documentation/2.5.x/JavaWS
 * @author veronez
 *
 */
public class WebRequestController extends Controller {

	private static final Logger logger = LoggerFactory.getLogger(WebRequestController.class);

	@Inject
	WSClient ws;

	@Inject
	Materializer materializer;

	public CompletionStage<Result> feeds() {
		logger.debug("feeds()");

		// "http://feeds.reuters.com/news/artsculture";
		final String feedUrl = "http://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fnews.ycombinator.com%2Frss";
		WSRequestFilter filter = executor -> {
			WSRequestExecutor next = request -> {
				logger.debug("url = {}", request.getUrl());
				return executor.apply(request);
			};
			return next;
		};

		return ws
				.url(feedUrl)
				.withRequestFilter(filter)
				.get()
				.thenApply(response -> {
					String title = response.asJson().findPath("title").toString();
					logger.debug("thenApply returning `ok` result");
					return ok("Feed title: "+ title);
				});
	}

	/**
	 * Controller handler implementation using {@link StreamedResponse} based
	 * on https://www.playframework.com/documentation/2.5.x/JavaWS#Processing-large-responses
	 * 
	 */
	public CompletionStage<Result> streamedResult() {
		logger.debug("streamedResult()");

		final String feedUrl = "http://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fnews.ycombinator.com%2Frss";
		CompletionStage<Result> result = ws.url(feedUrl).stream()
			.thenCompose(res -> {
				Source<ByteString, ?> responseBody = res.getBody();
				Sink<ByteString, CompletionStage<Long>> bytesSum =
						Sink.fold(0L, (total, bytes) -> total + bytes.length());
				return responseBody.runWith(bytesSum, materializer);
			})
			.thenApply(size -> ok("size: " + size))
			;
		return result;
	}

	/**
	 * Controller handler implementation using {@link StreamedResponse} based
	 * on https://www.playframework.com/documentation/2.5.x/JavaWS#Processing-large-responses
	 * 
	 */
	public CompletionStage<Result> consumedResponse() {
		logger.debug("consumedResponse()");

		final String feedUrl = "http://www.coolgames.com/";
		return ws.url(feedUrl).stream()
				.thenApply(WebRequestController::consumedResponse);
	}

	private static Result consumedResponse(StreamedResponse response) {
		logger.debug("consumedResponse({})", response);

		WSResponseHeaders headers = response.getHeaders();
		Source<ByteString, ?> body = response.getBody();

		final Result result;
		if (headers.getStatus() == OK) {
			String contentType = Optional.ofNullable(headers.getHeaders().get("Content-Type"))
					.map(types -> types.get(0))
					.orElse("application/octet-stream");

			Optional<String> contentLength = Optional.ofNullable(headers.getHeaders().get("Content-Length"))
					.map(types -> types.get(0));
			if (contentLength.isPresent()) {
				result = ok().sendEntity(new HttpEntity.Streamed(
						body,
						contentLength.map(Long::parseLong),
						Optional.of(contentType)
				));
			} else {
				result = ok().chunked(body).as(contentType);
			}
		} else {
			result = new Result(BAD_GATEWAY);
		}
		return result;
	}
}
