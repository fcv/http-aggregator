# HTTP Aggregator

[Play 2](https://www.playframework.com/) project which aims to asynchronously perform HTTP requests to multiple Web Pages and aggregate their response.

## Getting Started

This project's code has been initially created using [Lightbend Activator 1.3.0](https://www.lightbend.com/activator/download)'s template `play-java` following [Play's tutorial page](https://www.playframework.com/documentation/2.5.x/Tutorials). Example:

    $ activator new http-aggregator play-java

## Building and Deploying

This project uses [sbt](http://www.scala-sbt.org/) as main build tool.

Then it may be packaged using following command:

    $ sbt package

And executed using:

    $ sbt run

Once `sbt run` command finishes system may be accessed using URL http://localhost:9000/.